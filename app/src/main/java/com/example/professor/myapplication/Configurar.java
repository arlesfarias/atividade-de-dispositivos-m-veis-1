package com.example.professor.myapplication;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;

public class Configurar extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configurar);

        final SharedPreferences sharedPreferences = getSharedPreferences("My preferences", this.getApplicationContext().MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPreferences.edit();

        Button bt_ok = (Button) findViewById(R.id.bt_ok);

        bt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton bt_masc = (RadioButton) findViewById(R.id.radio_masc);
                RadioButton bt_fem = (RadioButton) findViewById(R.id.radio_fem);
                TextView nome = (TextView) findViewById(R.id.nome);
                CheckBox email = (CheckBox) findViewById(R.id.email);
                CheckBox sms = (CheckBox) findViewById(R.id.sms);
                CheckBox telefone = (CheckBox) findViewById(R.id.telefone);

                //false = masculino, true feminino
                boolean sex = false;

                if (bt_masc.isChecked() && !bt_fem.isChecked()) {
                    sex = false;
                } else if (!bt_masc.isChecked() && bt_fem.isChecked()) {
                    sex = true;
                }

                if(email.isChecked()){
                    editor.putBoolean("email", true);
                }else{
                    editor.putBoolean("email", false);
                }

                if(sms.isChecked()){
                    editor.putBoolean("sms", true);
                }else{
                    editor.putBoolean("sms", false);
                }

                if(telefone.isChecked()){
                    editor.putBoolean("telefone", true);
                }else{
                    editor.putBoolean("telefone", false);
                }

                editor.putBoolean("Sexo", sex);
                editor.putString("Nome", nome.getText().toString());

                editor.apply();

                finish();

            }
        });



        Button cancelar = (Button) findViewById(R.id.bt_cancel);
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_configurar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
