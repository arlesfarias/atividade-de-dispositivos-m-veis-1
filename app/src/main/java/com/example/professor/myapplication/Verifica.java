package com.example.professor.myapplication;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class Verifica extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verifica);

        final SharedPreferences sharedPreferences = getSharedPreferences("My preferences", this.getApplicationContext().MODE_PRIVATE);
        final TextView saudacao = (TextView) findViewById(R.id.tv_saudacao);
        final TextView nome = (TextView) findViewById(R.id.tv_nome);
        final TextView notificacoes = (TextView) findViewById(R.id.tv_notificacoes);
        boolean nenhumaNotificacao = true;

        boolean sexo = sharedPreferences.getBoolean("Sexo", false);

        if(sexo){
           saudacao.setText("Bom dia senhora");
        }else{
            saudacao.setText("Bom dia senhor");
        }

        nome.setText(sharedPreferences.getString("Nome", "Jorge Campos"));

        if(sharedPreferences.getBoolean("email", false)){
            notificacoes.append("E-mail\n");
            nenhumaNotificacao = false;
        }

        if(sharedPreferences.getBoolean("sms", false)){
            notificacoes.append("SMS\n");
            nenhumaNotificacao = false;
        }

        if(sharedPreferences.getBoolean("telefone", false)){
            notificacoes.append("Telefone\n");
            nenhumaNotificacao = false;
        }

        if(nenhumaNotificacao){
            notificacoes.setText("Você optou por não receber notificações.");
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_verifica, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
